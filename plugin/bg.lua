local update = function()
	local normal = vim.api.nvim_get_hl_by_name("TermBackground", true)
	local bg = normal["background"]
	local fg = normal["foreground"]

	local bghex = string.format("#%06x", bg)
	local fghex = string.format("#%06x", fg)

	os.execute("sed -i 's/background =.*$/background = \"" .. bghex .. "\"/' $HOME/.config/alacritty/alacritty.toml")
	os.execute("sed -i 's/foreground =.*$/foreground = \"" .. fghex .. "\"/' $HOME/.config/alacritty/alacritty.toml")
end

local setup = function()
	vim.api.nvim_create_autocmd({ "ColorScheme", "UIEnter" }, { callback = update })
end

setup()
